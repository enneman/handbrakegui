# handbrakegui 
 
##Summary
A simple gui wrapper for the handbrake (http://handbrake.fr) video encoder

##Usage

handbrakegui

##History

This script is an adaptation from a script I once found on a puppy linux forum
around 2009. 

http://www.murga-linux.com/puppy/viewtopic.php?p=310553#310553

My version is somewhat simplified, defaults to mp4 output,
and uses the builtin presets to set the needed options. It also
uses gtkdialog instead of gtkdialog3

##Dependencies
gtkdialog
gxmessage
gtklogviewer <---- This one is hard to find so I added it to the archive
