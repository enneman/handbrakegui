/*

* gtklogfileviewer, Mark Ulrich, jan. 2008, based on code of gwatch2
* License: Gpl V3 or later

 * gwatch2
 * $Log: main.c,v $
 * Revision 1.5  2005/05/16 16:23:15  ganter
 * doscroll tut besser
 *
 * Revision 1.4  2005/05/16 15:45:49  ganter
 * Versionsnummer in Begrüßung
 *
 * Revision 1.3  2005/05/16 15:40:29  ganter
 * scrolled jetzt richtig zum Ende
 *
 * Revision 1.2  2005/05/16 14:52:20  ganter
 * ...
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <glib.h>
#include <gtk/gtk.h>

#include <gdk/gdk.h>
#include <errno.h>

gchar *thefile;


int tcount;
gint fd1, fd2;
off_t fd1_p, fd2_p;
GtkWidget *textview1;
GtkTextBuffer *textbuffer;
GtkTextIter iter;
gchar fgcol[100], fg2col[100], bgcol[100], redcol[100];
GtkWidget *window1, *textview1;
gint needscroll;// = FALSE;


//#define GLADE_HOOKUP_OBJECT(component,widget,name) \
//  g_object_set_data_full (G_OBJECT (component), name, \
//    gtk_widget_ref (widget), (GDestroyNotify) gtk_widget_unref)

//#define GLADE_HOOKUP_OBJECT_NO_REF(component,widget,name) \
//  g_object_set_data (G_OBJECT (component), name, widget)


gint
neuertext (GtkWidget * widget, gpointer datum)
{
	off_t fd1_p_old;
	ssize_t e;
	gchar *buf;

	fd1_p_old = fd1_p;
	fd1_p = lseek (fd1, 0, SEEK_END);
	if (fd1_p > fd1_p_old)
	{
		buf = g_malloc (fd1_p - fd1_p_old + 1);
		lseek (fd1, fd1_p_old, SEEK_SET);
		e = read (fd1, buf, fd1_p - fd1_p_old);
		if (e != (fd1_p - fd1_p_old))
			g_print ("\nKonnte nur %d Bytes anstatt %d Bytes von %s lesen\n", (gint) e, (gint) (fd1_p - fd1_p_old), thefile);
		*(buf + fd1_p - fd1_p_old) = 0;

		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &iter,
							  g_locale_to_utf8(buf,strlen(buf),NULL,NULL,NULL), -1,
							  "blue_foreground",
							  "background", NULL);
		free (buf);
		needscroll = TRUE;
	}


	return (TRUE);
}

gint doscroll (GtkWidget *widget, gpointer datum);
gint doscroll (GtkWidget *widget, gpointer datum){
	if (needscroll)
	{
		gtk_text_buffer_get_end_iter (textbuffer, &iter);
		gtk_text_view_scroll_to_iter (GTK_TEXT_VIEW (textview1),
					      &iter, 0.0, 1, 1.0, 0);
		needscroll = FALSE;
	}

	//-- always scroll in the first 8 seconds, so that larger files have time to load and then the end is shown
	if (tcount < 8){
		gtk_text_buffer_get_end_iter (textbuffer, &iter);
		gtk_text_view_scroll_to_iter (GTK_TEXT_VIEW (textview1),
					      &iter, 0.0, 1, 1.0, 0);
	}
	tcount ++;


	return TRUE;
}

load_file(){
	ssize_t e;
	gchar *buf;
		buf = g_malloc (fd1_p + 1);
		lseek ( fd1, 0,SEEK_SET);
		e = read (fd1, buf, fd1_p);
		if (e != (fd1_p))
			g_print ("\nKonnte nur %d Bytes anstatt %d Bytes von %s lesen\n", (gint) e, (gint) (fd1_p), thefile);
		*(buf + fd1_p) = 0;

		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &iter,
							  g_locale_to_utf8(buf,strlen(buf),NULL,NULL,NULL), -1,
							  "blue_foreground",
							  "background", NULL);
		free (buf);


		gtk_text_buffer_get_end_iter (textbuffer, &iter);
		gtk_text_view_scroll_to_iter (GTK_TEXT_VIEW (textview1),
					      &iter, 0.0, 1, 1.0, 0);

		needscroll = TRUE;
}


void
openfiles (void)
{
	fd1 = open (thefile, O_RDONLY);
	if (fd1 < 0)
	{
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &iter,
							  ("Error opening "),
							  -1,
							  "yellow_foreground",
							  "background", NULL);
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &iter,
							  thefile, -1,
							  "yellow_foreground",
							  "background", NULL);
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &iter,
							  ":  ", -1,
							  "yellow_foreground",
							  "background", NULL);
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &iter,
							  strerror (errno),
							  -1,
							  "yellow_foreground",
							  "background", NULL);
		gtk_text_buffer_insert_with_tags_by_name (textbuffer, &iter,
							  "\n", -1,
							  "yellow_foreground",
							  "background", NULL);

	}


	fd1_p = lseek (fd1, 0, SEEK_END);
load_file();


}
GtkWidget*
lookup_widget                          (GtkWidget       *widget,
                                        const gchar     *widget_name)
{
  GtkWidget *parent, *found_widget;

  for (;;)
    {
      if (GTK_IS_MENU (widget))
        parent = gtk_menu_get_attach_widget (GTK_MENU (widget));
      else
        parent = widget->parent;
      if (!parent)
        parent = (GtkWidget*) g_object_get_data (G_OBJECT (widget), "GladeParentKey");
      if (parent == NULL)
        break;
      widget = parent;
    }

  found_widget = (GtkWidget*) g_object_get_data (G_OBJECT (widget),
                                                 widget_name);
  if (!found_widget)
    g_warning ("Widget not found: %s", widget_name);
  return found_widget;
}
static button_clicked( GtkWidget *widget,
                   gpointer   data )
{
    g_print ("exitbutton\n");
    exit(0);
}
static window_closed( GtkWidget *widget,
                   gpointer   data )
{
    g_print ("windowclosed\n");
    exit(0);
}

int
main (int argc, char *argv[])
{
	char hn[81];
	GString *title;
	PangoFontDescription *font_desc;

	gchar *str, font[100];
	GdkColor color;
	GtkWidget *eventbox1;
	gint pid;
	gchar string[200];

	gchar *buttontext;
	buttontext = argv[2];
gchar *sx;
gchar *sy;
gchar *wx;
gchar *wy;

sx = argv[3];
sy = argv[4];
wx = argv[5];
wy = argv[6];

int x , y , w , h;

int usecenter = 0;
if ( ! strcmp (sx , "-center") ) {

	usecenter = 1;
}else{
	x = atoi(sx);
	y = atoi(sy);
}
w = atoi(wx);
h = atoi(wy);

thefile = argv[1];

#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif
	strcpy (font, "Courier 11");
	strcpy (fgcol, "black");
	strcpy (fg2col, "black");
	strcpy (redcol, "red");

	strcpy (bgcol, "white");

gtk_init(&argc, &argv);
GdkColor schwarz = { 0, 0x00, 0x00, 0x00 };
GdkColor gruen = { 0, 0x00, 0xffff, 0x0000 };
GdkColor gelb = { 0, 0xe000, 0xd500, 0x0b00 };
GtkStyle *style = NULL;
GtkWidget *text;
gint i = 0;

  GtkWidget *scrolledwindow1;
  GtkWidget *vbox1;
GtkWidget *button1;

  window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
gtk_window_set_position (GTK_WINDOW (window1),GTK_WIN_POS_CENTER);
if(usecenter == 0){
	gtk_widget_set_uposition(GTK_WIDGET (window1) ,x,y);
}
  gtk_window_set_title (GTK_WINDOW (window1), (thefile));
  gtk_window_set_default_size (GTK_WINDOW (window1), w, h);

  gtk_window_set_gravity (GTK_WINDOW (window1), GDK_GRAVITY_NORTH_EAST);
  gtk_container_set_border_width (GTK_CONTAINER (window1), 10);
vbox1 = gtk_vbox_new (FALSE, 10);
  gtk_container_add (GTK_CONTAINER (window1), vbox1);
  gtk_widget_show (vbox1);
  eventbox1 = gtk_event_box_new ();
  gtk_widget_show (eventbox1);
gtk_box_pack_start (GTK_BOX (vbox1), eventbox1, 1, 1, 0);
  scrolledwindow1 = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_show (scrolledwindow1);
  gtk_container_add (GTK_CONTAINER (eventbox1), scrolledwindow1);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow1), GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);
gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolledwindow1),GTK_SHADOW_IN);
  textview1 = gtk_text_view_new ();
  gtk_widget_show (textview1);
  gtk_container_add (GTK_CONTAINER (scrolledwindow1), textview1);
  //gtk_widget_set_size_request (textview1, 580, 160);
  gtk_text_view_set_editable (GTK_TEXT_VIEW (textview1), FALSE);
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (textview1), GTK_WRAP_WORD);
  gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (textview1), FALSE);
  gtk_text_view_set_left_margin (GTK_TEXT_VIEW (textview1), 4);
  gtk_text_view_set_right_margin (GTK_TEXT_VIEW (textview1), 4);

//button1 = gtk_button_new_from_stock ("gtk-cancel");
GtkWidget *image1;
GtkWidget *hbox1;
GtkWidget *label1;
image1 = gtk_image_new_from_stock ("gtk-cancel" , GTK_ICON_SIZE_BUTTON);
button1 = gtk_button_new_with_label (buttontext);
/*
label1 = gtk_label_new ("kill");
hbox1 = gtk_hbox_new (FALSE, 0);
gtk_container_add (GTK_CONTAINER (button1), hbox1);
gtk_box_pack_start (GTK_BOX (hbox1), image1, 0, 0, 0);
gtk_box_pack_start (GTK_BOX (hbox1), label1, 0, 0, 0);
*/
//gtk_button_set_use_stock(button1 , 0);
gtk_button_set_image (GTK_BUTTON(button1) , image1);
//gtk_button_set_label (button1 , "gtk-ok");
gtk_box_pack_start (GTK_BOX (vbox1), button1, 0, 1, 0);
  gtk_widget_show (button1);
//  gtk_widget_show (hbox1);
  gtk_widget_show (image1);
//  gtk_widget_show (label1);


  g_signal_connect (G_OBJECT (button1), "clicked",
		      G_CALLBACK (button_clicked), NULL);

  g_signal_connect ((gpointer) window1, "delete_event",
                    G_CALLBACK (window_closed),
                    NULL);
  g_signal_connect ((gpointer) window1, "destroy_event",
                    G_CALLBACK (window_closed),
                    NULL);

  /* Store pointers to all widgets, for use by lookup_widget(). */
//  GLADE_HOOKUP_OBJECT_NO_REF (window1, window1, "window1");
//  GLADE_HOOKUP_OBJECT (window1, eventbox1, "eventbox1");
//  GLADE_HOOKUP_OBJECT (window1, scrolledwindow1, "scrolledwindow1");
//  GLADE_HOOKUP_OBJECT (window1, textview1, "textview1");



	gtk_widget_show (window1);
	gethostname (hn, 80);
	title = g_string_new ("");
	g_string_sprintf (title, thefile, hn);
	gtk_window_set_title (GTK_WINDOW (window1), title->str);
//	textview1 = lookup_widget (window1, "textview1");
	textbuffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview1));
	/* Change default font throughout the widget */
	font_desc = pango_font_description_from_string (font);
	gtk_widget_modify_font (textview1, font_desc);
	pango_font_description_free (font_desc);

	/* Change default color throughout the widget */
	gdk_color_parse (bgcol, &color);
	//eventbox1 = lookup_widget (window1, "eventbox1");
	gtk_widget_modify_base (GTK_WIDGET (textview1), GTK_STATE_NORMAL,
				&color);
	gtk_widget_modify_bg (eventbox1, GTK_STATE_NORMAL, &color);


	gtk_text_buffer_get_end_iter (textbuffer, &iter);
	gtk_text_buffer_create_tag (textbuffer, "blue_foreground",
				    "foreground", fgcol, NULL);
	gtk_text_buffer_create_tag (textbuffer, "yellow_foreground",
				    "foreground", fg2col, NULL);
	gtk_text_buffer_create_tag (textbuffer, "red_foreground",
				    "foreground", redcol, NULL);

	gtk_text_buffer_create_tag (textbuffer, "background",
				    "background", bgcol, NULL);

	gtk_text_buffer_insert_with_tags_by_name (textbuffer, &iter, string,
						  -1, "blue_foreground",
						  "background", NULL);

	openfiles ();

	tcount = 0;
	gtk_timeout_add (500, (GtkFunction) neuertext, NULL);
	gtk_timeout_add (1000, (GtkFunction) doscroll, NULL);

	if ((pid = fork ()) < 0)
		return -1;
	else if (pid != 0)
		exit (0);

	gtk_main ();
	return 0;
}
