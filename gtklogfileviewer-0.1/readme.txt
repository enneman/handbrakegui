gtklogfileviewer

*-------------------------------------------------------------------*
This program displays a logfile and updates the view when the file gets bigger.
It does not reload the whole file, but only the new lines.
So it should not use too many resources. 

*-------------------------------------------------------------------*
This is a complete rewrite of gwatch2 , see: http://mmm.berklix.net/backup/gpsdrive.cc/www.gpsdrive.cc/gwatch.html

What is different:

- It will display the whole file by default.
  gwatch2 just displays the changes on a file, so you do not see the first lines.

- It has less dependencies, Gtk2 only.
  Gwatch2 also needs gnome and libglade

- It uses "Courier" as font for the textdisplay
- It converts files from your locale to utf-8 (logs are usually not encoded in utf-8, but the textview needs it)

- the code looks dirty. Sorry, I'm tired.


*-------------------------------------------------------------------*
Usage: gtklogfileviewer logfile buttontext x y width height

Examples:
gtklogfileviewer /var/log/messages exit 0 0 500 200
gtklogfileviewer /tmp/xerrs.txt "auf Wiedersehen" "-center" 0 300 200

x is the position from the left side of the screen
y is the position from the top side of the screen

x also can be "-center", then the value of y is ignored.


*-------------------------------------------------------------------*
Returnvalues:
If the button is clicked, "exitbutton" is printed to stdout.
If the window is closed, "windowclosed" is printed to stdout.

You can get that value from shellscripts like this:

gtklogfileviewer /var/log/messages exit 0 0 500 200
result=$?
echo $result


*-------------------------------------------------------------------*
This archive includes main.c Makefile gtklogfileviewer and readme.txt
Copy gtklogfileviewer to /usr/local/bin/ 
If it does not run, compile it again by typing "make".

Mark Ulrich, MarkUlrich@gmx.de Jan, 16, 2008

